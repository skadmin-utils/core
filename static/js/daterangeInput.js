$(function () {
    $.nette.ext({
        success: function () {
            daterangeInput();
        },
        init: function () {
            daterangeInput();
        }
    });

    function daterangeInput() {
        // DATE-DATE-RANGE
        $('[data-daterange]:not(.daterange-init)').each(function () {
            var format = $(this).data('daterange') ? $(this).data('daterange') : 'DD.MM.YYYY';

            var setting = {
                locale: {
                    format: format,
                    applyLabel: "Potvrdit",
                    cancelLabel: "Zrušit"
                }
            };

            if ($(this).data('daterange-mindate')) {
                setting.minDate = $(this).data('daterange-mindate');
            }

            if ($(this).data('daterange-timepicker') !== undefined) {
                setting.timePicker = true;
                setting.timePicker24Hour = true;
            }

            if ($(this).data('date-opens')) {
                setting.opens = $(this).data('date-opens');
            }

            if ($(this).data('date-drops')) {
                setting.drops = $(this).data('date-drops');
            }

            $(this)
                .addClass('daterange-init')
                .daterangepicker(setting);

            // icons
            $(this).wrap('<div class="input-group"></div>');
            $('<div class="input-group-prepend"><label class="input-group-text" for="' + $(this).attr('id') + '"><i class="far fa-calendar-alt"></i></label></div>').insertBefore($(this));
        });

        // DATE-DATE

        $('[data-date]:not(.date-init)').each(function () {
            var clearInput = false;
            var clearInputButton = false;

            if ($(this).data('date-clear') !== undefined) {
                clearInputButton = true;

                if (!$(this).val()) {
                    clearInput = true;
                }
            }

            var format = $(this).data('date') ? $(this).data('date') : 'DD.MM.YYYY';

            var setting = {
                locale: {
                    format: format,
                    applyLabel: "Potvrdit",
                    cancelLabel: "Zrušit"
                }
            };

            if ($(this).data('date-mindate')) {
                setting.minDate = $(this).data('date-mindate');
            }

            setting.singleDatePicker = true;
            setting.showDropdowns = true;

            if ($(this).data('date-opens')) {
                setting.opens = $(this).data('date-opens');
            }

            if ($(this).data('date-drops')) {
                setting.drops = $(this).data('date-drops');
            }

            $(this)
                .addClass('date-init')
                .daterangepicker(setting);

            // icons
            if ($(this).data('date-noicon') === undefined) {
                $(this).wrap('<div class="input-group"></div>');
                $(this).addClass('border-left-0');
                $('<div class="input-group-prepend"><label class="input-group-text bg-transparent" for="' + $(this).attr('id') + '"><i class="far fa-fw fa-calendar-alt"></i></label></div>').insertBefore($(this));
            }

            console.log(clearInputButton);
            if (clearInputButton) {
                $(this).addClass('border-right-0');
                $('<div class="input-group-append"><a href="#" class="input-group-text bg-transparent"><i class="fas fa-fw fa-times fa-xs text-danger opacity-75"></i></></div>')
                    .insertAfter($(this))
                    .click(function () {
                        $(this).prev().val('');
                        return false;
                    });
            }

            if (clearInput) {
                $(this).val('');
            }
        });
    }
});

