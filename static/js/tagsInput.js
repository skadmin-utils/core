$(function () {
    const TAGS_INPUT_CONFIG = {
        default: {
            delimiter: ';',
            trimValue: true,
            tagClass: function (item) {
                return 'badge badge-primary';
            }
        },
        email: {
            confirmKeys: [13, 9, 32, 192],
            tagClass: function (item) {
                let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                if (regex.test(item)) {
                    return 'badge badge-primary';
                }

                return 'badge badge-danger';
            }
        }
    };

    $.nette.ext({
        success: function () {
            tagsInputInit();
        },
        init: function () {
            tagsInputInit();
        }
    });

    function tagsInputInit() {
        $('[data-tagsinput-type]:not(.tagsinput-init)').each(function () {
            var key = $(this).data('tagsinput-type');

            let tagsInputOption = TAGS_INPUT_CONFIG['default'];
            if (key in TAGS_INPUT_CONFIG) {
                $.extend(tagsInputOption, TAGS_INPUT_CONFIG[key]);
            }

            let modifyOptions = {};

            $.extend(tagsInputOption, modifyOptions);

            $(this).addClass('tagsinput-init')
                .tagsinput(tagsInputOption);
        });
    }
});
