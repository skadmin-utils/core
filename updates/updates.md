# 10.05.2021

## Move mailing to package

> DELETE migrations
```
Version20190821092423
Version20190822074637
Version20200121065456
Version20191213120221
```

> ### Remove
> - files from model/mail exclude [MailClassBox.php, CMail*.php, templates]
> - modules/AdminModule/components/grid/GridMailTemplate.php
> - modules/AdminModule/components/form/formMailTemplate
> - modules/AdminModule/presenters/MailPresenter.php
> - modules/AdminModule/templates/Mail
> - app/model/doctrine/mail

> ### Remove service from `app/config/common.neon`
> `App\Model\Mail\MailService`

> MODIFY app/modules/adminModule/templates/_block/sidebar.latte
```latte
<li n:class="nav-item, ($presenter->isLinkCurrent('Component:default') && get_class($currentPackage) === Skadmin\Mailing\BaseControl::class) ? active" n:if="$user->isAllowed(Skadmin\Mailing\BaseControl::RESOURCE, Skadmin\Role\Doctrine\Role\Privilege::READ)">
    <a class="nav-link" n:href="Component:default package => (new Skadmin\Mailing\BaseControl()), render => 'overview'"><i class="fas fa-fw fa-mail-bulk"></i> <span>{_'mail.default'}</span></a>
</li>
```

> MODIFY app/modules/adminModule/presenters/CoreComponentPresenter.php
```php
    protected function prepareControl(?ABaseControl $package, ?string $_render, ?int $id, string $module = ABaseControl::MODULE_ADMIN) : void
    {
        $render       = Strings::firstLower(SystemStrings::camelize($_render));
        $controlClass = $package->getControlClass($module, $render);

        $this->template->currentPackage = $package;
    ...
    }
```
> Replace
```
from "App\Model\Doctrine\Mail\MailFacade"
to "Skadmin\Mailing\Doctrine\Mail\MailFacade"

from "App\Model\Doctrine\Mail\MailQueue"
to "Skadmin\Mailing\Doctrine\Mail\MailQueue"

from "App\Model\Doctrine\Mail\MailTemplate"
to "Skadmin\Mailing\Doctrine\Mail\MailTemplate"

from "App\Model\Mail\MailService"
to "Skadmin\Mailing\Model\MailService" 

from "App\Model\Mail\MailParameterValue"
to "Skadmin\Mailing\Model\MailParameterValue"

from "App\Model\Mail\MailTemplateParameter"
to "Skadmin\Mailing\Model\MailTemplateParameter"

from "App\Model\Mail\CMail"
to "Skadmin\Mailing\Model\CMail"
```

> RUN SQL
```sql
DELETE FROM _nettrine_migrations 
    WHERE version IN (
        20190821092423, 
        20190822074637,
        20200121065456,
        20191213120221
    );

INSERT INTO _nettrine_migrations_skadmin (version, executed_at) VALUES
    (20190821092423, CURRENT_TIMESTAMP),
    (20190822074637, CURRENT_TIMESTAMP);

INSERT INTO _nettrine_migrations_after (version, executed_at) VALUES
    (20190821092424, CURRENT_TIMESTAMP);
```

> Move migrations from source/migrations-10052021 and rename NAMESPACE

> modify TSideMenu
```php
if (count($items) === 0) {
    continue;
} elseif (count($items) === 1) {
    $_menu = $this->createSideMenuLi($package, $controlMenu->control, $items[0], $controlMenu->icon);
}
```

# TEST! => UPLOAD AND TEST!

---
# 08.05.2021

## Form callabck
> app/components/form/formSign/FormLogIn.php
> app/components/form/FormControl.php
> app/components/form/Form.php

Add `= []` for all callback
```php
/** @var callable[] */
public $onBack = [];
```

## CoreComponentPresenter
> app/modules/adminModule/presenters/CoreComponentPresenter.php

from
```php
::33 $control = $this->context->getByType($controlClass);
```

to 
```php
/** @var Container @inject */
public $container;

$control = $this->container->getByType($controlClass);
```

## hold toggle sidebar
add cookie saver for information about toogle menu

#### JQuery cookies
> www/3rd/jquery-cookies/jquery.cookie.js

add `jquery-cookies` to 3rd

#### Sidebar template
> app/modules/adminModule/templates/_block/sidebar.latte`
```latte
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion {isset($_COOKIE['_skadminSidebar']) && $_COOKIE['_skadminSidebar'] === 'true' ? 'toggled'}" id="menu-sidebar">
```

#### SB-ADMIN js
> www/_admin/js/sb-admin-2.js
```js
 // Toggle the side navigation
 $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {

   $("body").toggleClass("sidebar-toggled");
   $(".sidebar").toggleClass("toggled");
   if ($(".sidebar").hasClass("toggled")) {
     $('.sidebar .collapse').collapse('hide');
   };

   $.cookie('_skadminSidebar', $(".sidebar").hasClass("toggled"), {path: '/'});
 });
```

#### WebloadAdmin
> app/config/includes/webloader.admin.neon
```neon
webloader:
  *
  js:
    adminFoot:
      files:
        *  
        - %path3rd%/jquery-cookies/jquery.cookie.js
        *
```

---
## update link in responsive filemanager

from `//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js`
to `//cdnjs.cloudflare.com/ajax/libs/blueimp-load-image/2.18.0/load-image.all.min.js`

```html
dialog.php:364
----------
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/blueimp-load-image/2.18.0/load-image.all.min.js"></script>
```