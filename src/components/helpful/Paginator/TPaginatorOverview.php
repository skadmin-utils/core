<?php

declare(strict_types=1);

namespace SkadminUtils\Core\Components\Helpful;

trait TPaginatorOverview
{
    private Paginator $paginator;

    public int $page = 1;

    protected function createComponentPaginator(): Paginator
    {
        $this->paginator->onChangePage[] = [$this, 'paginatorOnChangePage'];

        return $this->paginator;
    }

    public function paginatorOnChangePage(int $page): void
    {
        $this->page = $page;
        $this->paginator->setPage($page);
        if ($this->getPresenterIfExists()) {
            $this->getPresenter()->payload->url = $this->createLink();
        }

        $this->redrawControl('snipOverview');
    }

    public function createLink(): string
    {
        return $this->link('this', ['page' => $page]);
    }
}
