<?php

declare(strict_types=1);

namespace SkadminUtils\Core\Components\Helpful;

interface IPaginatorFactory
{
    public function create(): Paginator;
}
