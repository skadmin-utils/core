<?php

declare(strict_types=1);

namespace SkadminUtils\Core\Components\Helpful;

use Nette\Application\UI\Control;
use Nette\Utils\Paginator as NUtilsPAginator;
use Skadmin\Translator\Translator;

use function intval;

class Paginator extends Control
{
    /** @var callable[]  function (?int $page); Occurs when the form is submitted and successfully validated */
    public array $onChangePage = [];

    private Translator $translator;

    private NUtilsPAginator $paginator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function render(): void
    {
        $this->template->setFile(__DIR__ . '/simple.latte');
        $this->template->setTranslator($this->translator);
        $this->template->paginator = $this->paginator;
        $this->template->cName     = $this->name;
        $this->template->render();
    }

    public function handleChangePage(string $page = '1'): void
    {
        $this->onChangePage(intval($page));
    }

    public function getItemsPerPage(): int
    {
        return $this->getPaginator()->getItemsPerPage();
    }

    public function getPaginator(): NUtilsPAginator
    {
        return $this->paginator;
    }

    public function setPaginator(int $itemCount, int $itemsPerPage, int $page): void
    {
        $this->paginator = new NUtilsPAginator();
        $this->paginator->setItemCount($itemCount);
        $this->paginator->setItemsPerPage($itemsPerPage);
        $this->paginator->setPage($page);
    }

    public function setPage(int $page): NUtilsPAginator
    {
        $this->getPaginator()->setPage($page);

        return $this->getPaginator();
    }

    public function setItemCount(int $itemCount): NUtilsPAginator
    {
        $this->getPaginator()->setItemCount($itemCount);

        return $this->getPaginator();
    }

    public function getOffset(): int
    {
        return $this->getPaginator()->getOffset();
    }

    public function getPage(): int
    {
        return $this->getPaginator()->getPage();
    }
}
