<?php

declare(strict_types=1);

namespace SkadminUtils\Core\Components\Grid;

use App\Model\Doctrine\UserLog\CustomerLog;
use App\Model\System\Constant;
use Doctrine\ORM\QueryBuilder;
use Nette\Security\User;
use Nette\Utils\Html;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

use function array_merge_recursive;
use function is_string;

class GridOverviewLog extends GridControl
{
    private QueryBuilder $model;

    /** @var array<string, string> */
    private array $types;

    /**
     * @param array<mixed> $types
     */
    public function __construct(QueryBuilder $queryBuilder, array $types, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);

        $this->model = $queryBuilder;
        $this->types = $types;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/gridOverviewLog.latte');

        $template->render();
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->model);

        // COLUMNS
        $grid->addColumnText('id', '#')
            ->setAlign('center')
            ->setDefaultHide(true);
        $grid->addColumnText('type', 'core.grid.overview.log.type')
            ->setRenderer(function ($log): Html {
                $result = new Html();
                $result->addHtml($this->translator->translate($log->getType()))
                    ->addHtml('<br>')
                    ->addHtml(Html::el('code', ['class' => 'text-muted small'])
                        ->setText($log->getCreatedAt()->format('d.m.Y H:i')));

                return $result;
            });
        $grid->addColumnText('author', 'core.grid.overview.log.author');
        $grid->addColumnText('params', 'core.grid.overview.log.params')
            ->setRenderer(function (CustomerLog $log): Html {
                $result = new Html();

                $counter = 0;
                foreach ($log->getParams() as $key => $params) {
                    $div = Html::el('div');

                    if (isset($params[0])) {
                        $params[0] = $this->translator->translate($params[0]);
                    }

                    if (is_string($key)) {
                        $params = array_merge_recursive([$key], $params);
                    }

                    $div->setHtml($this->translator->translate(new SimpleTranslation($log->getMessage($counter++), $params)));

                    $result->addHtml($div);
                }

                return $result;
            });

        // FILTER - problem when is in form...
        $grid->addFilterSelect('type', 'core.grid.overview.log.type', Constant::PROMTP_ARR + $this->types);
        $grid->addFilterText('author', 'core.grid.overview.log.author');
        $grid->addFilterText('params', 'core.grid.overview.log.params');

        // OTHER
        $grid->setDefaultSort(['id' => 'DESC']);

        return $grid;
    }
}
