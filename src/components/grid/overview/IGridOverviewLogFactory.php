<?php

declare(strict_types=1);

namespace SkadminUtils\Core\Components\Grid;

use Doctrine\ORM\QueryBuilder;

interface IGridOverviewLogFactory
{
    /**
     * @param array<mixed> $types
     */
    public function create(QueryBuilder $queryBuilder, array $types = []): GridOverviewLog;
}
