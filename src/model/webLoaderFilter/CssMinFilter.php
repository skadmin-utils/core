<?php

declare(strict_types=1);

namespace SkadminUtils\Core\Model\WebLoaderFilter;

use MatthiasMullie\Minify\CSS;
use WebLoader\Compiler;

use function strpos;

class CssMinFilter
{
    public function __invoke(string $code, Compiler $loader, string $file = ''): string
    {
        $file = (string) $file;

        if (strpos($file, '.min.') !== false) {
            return $code;
        }

        return (new CSS($code))->minify();
    }

    ///**
    // * Minify target code
    // * @param string $code
    // * @param Compiler $compiler
    // * @param string $file
    // * @return string
    // */
    //public function __invoke($code, Compiler $compiler, $file = '')
    //{
    //    if (strpos($file, '.min.') !== FALSE) {
    //        return $code;
    //    }
    //    return CssMin::minify($code, array("remove-last-semicolon"));
}
