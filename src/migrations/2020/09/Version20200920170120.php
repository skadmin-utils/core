<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200920170120 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // $translations = [
        //     // ['original' => 'edit.tab.general','hash' => 'ef734c8140cfcccb3b6ec1ff2a4d7d05','module' => 'admin','language_id' => 1,'singular' => 'Základní údaje','plural1' => '','plural2' => ''],
        // ];

        // foreach ($translations as $translation) {
        //     $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
        //     $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        // }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
